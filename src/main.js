import Vue from 'vue'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import './plugins/vuetify'
import App from './App.vue'
import Db from './plugins/db'
import router from './router'
import store from './store'
import './registerServiceWorker'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faUserSecret, faTable, faCog, faCube, faTrash, faRandom, faHeart } from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faGoogle, faTwitter, faStrava, faAdn } from '@fortawesome/free-brands-svg-icons'

Vue.config.productionTip = false
library.add(faUserSecret, faTable, faCog, faCube, faFacebook, faGoogle, faTwitter, faTrash, faRandom, faHeart, faStrava, faAdn)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false
Vue.use(Db)
Vue.use(VueChartkick, {adapter: Chart})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
