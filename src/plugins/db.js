import PouchDB from 'pouchdb'
import PouchdDBFind from 'pouchdb-find'

PouchDB.plugin(PouchdDBFind)

const Db = {}
Db.install = function (Vue) {
  const db = new PouchDB('koperasi')
  db.createIndex({
    index: {
      fields: ['dataset', 'nama']
    }
  })
  Vue.prototype.$db = db
}

export default Db
