import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/app',
      name: 'app',
      component: () => import('./views/app/App.vue'),
      children: [
        {
          path: 'data/list',
          name: 'data',
          component: () => import('./views/app/data/list.vue')
        },
        {
          path: 'data/import',
          name: 'import',
          component: () => import('./views/app/data/jsonim.vue')
        },
        {
          path: 'data/summary-category',
          name: 'summary',
          component: () => import('./views/app/data/summary.vue')
        },
        {
          path: 'settings',
          name: 'settings',
          component: () => import('./views/app/settings.vue')
        },
        {
          path: 'testing-single',
          name: 'testing-single',
          component: () => import('./views/app/testing/single.vue')
        },
        {
          path: 'testing-kfold',
          name: 'testing-kfold',
          component: () => import('./views/app/testing/kfold.vue')
        }
      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
