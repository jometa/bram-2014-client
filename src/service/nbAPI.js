import axios from './axios'

export async function nb(dataset, payload) {
  const response = await axios.post(`/nb/${dataset}`, payload)
  const data = response.data
  return data
}

export default {
  nb
}
