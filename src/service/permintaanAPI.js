import axios from './axios'

async function create(payload) {
  const response = await axios.post('/permintaan', payload)
  return response.data
}

export async function update(id, payload) {
  let data = { ...payload }
  delete data._id
  const response = await axios.put(`/permintaan/${id}`, data)
  return response.data
}

export async function remove(id) {
  await axios.delete(`/permintaan/${id}`)
}

export async function find(dataset) {
  const config = {
    params: {
      dataset
    }
  }
  const response = await axios.get('/permintaan', config)
  return response.data
}

export async function findById(id) {
  const response = await axios.get(`/permintaan/${id}`)
  return response.data
}

export async function exportFile(dataset, file) {
  const response = await axios.post(`/import/${dataset}`, file)
  return response.data
}

export async function summary(dataset) {
  const response = await axios.get(`/summary/${dataset}`)
  return response.data
}

export async function count(dataset) {
  const response = await axios.get(`/count/${dataset}`)
  return response.data.count
}

export async function removeDataset(dataset) {
  await axios.delete(`/dataset/${dataset}`)
}

export default {
  create,
  update,
  remove,
  find,
  findById,
  exportFile,
  summary,
  count,
  removeDataset
}
