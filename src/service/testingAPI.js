import axios from './axios'

export async function kfold(dataset, k) {
  const response = await axios.post(`/testing`, {
    dataset,
    k
  })
  return response.data
}

export default {
  kfold
}
