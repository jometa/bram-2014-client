import { nbWithSumm } from './nb'
import { summary } from './summary'

export function testing (training, testing) {
  const summ = summary(training)
  let testingResult = {
    tp: 0,
    tn: 0,
    fp: 0,
    fn: 0
  }
  testing.forEach(x => {
    const actual = x.keterangan
    const nbResult = nbWithSumm(summ, x)
    const predicted = nbResult.result

    // TP
    if (actual === 1 && predicted === 1) {
      testingResult.tp += 1
    // TN
    } else if (actual === 0 && predicted === 0) {
      testingResult.tn += 1
    // FP
    } else if (actual === 0 && predicted === 1) {
      testingResult.fp += 1
    // FN
    } else if (actual === 1 && predicted === 0) {
      testingResult.fn += 1
    }
  })
  
  return testingResult
}

export const accuracy = testR => (testR.tp + testR.tn) * 100.0 / (testR.tp + testR.tn + testR.fp + testR.fn)
export const precision = testR => testR.tp * 100.0 / (testR.tp + testR.tn)
export const recall = testR => testR.tp * 100.0 / (testR.tp + testR.fn)
export const miscRate = testR => (testR.fp + testR.fn) * 100.0 / (testR.tp + testR.tn + testR.fp + testR.fn)
