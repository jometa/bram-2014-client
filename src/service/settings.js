const PREFIX = 'bram.koperasi'

export default {
  getMainName () {
    const mainName = localStorage.getItem(`${PREFIX}:mainName`)
    console.log('mainName=', mainName)
    if (!mainName) {
      return 'Teseract'
    }
    return mainName
  },

  setMainName (v) {
    localStorage.setItem(`${PREFIX}:mainName`, v)
  },

  getSecondaryName () {
    const secondaryName = localStorage.getItem(`${PREFIX}:secondaryName`)
    if (!secondaryName) {
      return 'Koperasi'
    }
    return secondaryName
  },

  setSecondaryName (v) {
    localStorage.setItem(`${PREFIX}:secondaryName`, v)
  },

  getTitle () {
    const title = localStorage.getItem(`${PREFIX}:title`)
    if (!title) {
      return 'Sistem Pendukung K Penentuan'
    }
    return title
  },

  setTitle (v) {
    localStorage.setItem(`${PREFIX}:title`, v)
  },

  getSubtitle () {
    const subtitle = localStorage.getItem(`${PREFIX}:subtitle`)
    if (!subtitle) {
      return 'STUDI KASUS ...'
    }
    return subtitle
  },

  setSubtitle (v) {
    localStorage.setItem(`${PREFIX}:subtitle`, v)
  },

  getActiveDataset () {
    const activeDataset = localStorage.getItem(`${PREFIX}:activeDataset`)
    if (!activeDataset) {
      return 'training'
    }
    return activeDataset
  },

  setActiveDataset (v) {
    localStorage.setItem(`${PREFIX}:activeDataset`, v)
  },

  getOptionsDataset () {
    const optionsDataset = localStorage.getItem(`${PREFIX}:optionsDataset`)
    if (!optionsDataset) {
      return ['training', 'testing']
    }
    return JSON.parse(optionsDataset)
  },

  setOptionsDataset (vs) {
    localStorage.setItem(`${PREFIX}:optionsDataset`, vs)
  },

  addOptionsDataset (v) {
    let options = this.getOptionsDataset()
    options.push(v)
    this.setOptionsDataset(options)
  }
}
