export const optionsDataset = ['training', 'testing']
export const defaultDataset = 'training'

export default {
  optionsDataset,
  defaultDataset
}
